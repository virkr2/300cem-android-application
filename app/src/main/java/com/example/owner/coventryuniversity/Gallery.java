package com.example.owner.coventryuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Gallery extends AppCompatActivity {
    private ListView listView;
    private String[] buildingNames;

    //method for content view of page
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
        buildingNames = getResources().getStringArray(R.array.buildingNames);

        listView = (ListView) findViewById(R.id.listViewSimple);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, buildingNames);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getBaseContext(), buildingNames[position] + "", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    //switch case to start either the list view or grid view activity
    public void onButtonClick(View v) {
        switch (v.getId()) {
            case R.id.complexList:
                startActivity(new Intent(this, PhotoListActivity.class));
                break;
            case R.id.gridView:
                startActivity(new Intent(this, GridActivity.class));
                break;
        }
    }
}