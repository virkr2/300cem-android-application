package com.example.owner.coventryuniversity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotoListActivity extends AppCompatActivity {

    private ListView listView;
    private String[] buildingNames;
    private String[] buildingDetails;
    public static int[] buildingPhotos = {
            //pictures and photos defined in the drawable folder
            R.drawable.alanberry,
            R.drawable.armstrong,
            R.drawable.ec,
            R.drawable.library,
            R.drawable.hub,
            R.drawable.jaguar,
            R.drawable.carpark,
            R.drawable.centre,
            R.drawable.sports
    };
    private ArrayList<Building> buildings = new ArrayList<>();

    //method for photo list activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);

        buildingNames = getResources().getStringArray(R.array.buildingNames);
        buildingDetails = getResources().getStringArray(R.array.buildingDetails);
        generateBuilding();

        //list view for the layout of images
        listView = (ListView) findViewById(R.id.listViewComplex);
        listView.setAdapter(new BuildingAdapter(this, R.layout.list_item, buildings));
        listView.setOnItemClickListener(

                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Toast.makeText(getBaseContext(), "" + buildings.get(position), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void generateBuilding() {

        for (int i = 0; i < buildingPhotos.length; i++) {
            buildings.add(new Building(buildingNames[i], buildingDetails[i], buildingPhotos[i]));
        }
    }
}