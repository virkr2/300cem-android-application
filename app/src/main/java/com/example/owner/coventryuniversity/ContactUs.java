package com.example.owner.coventryuniversity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class ContactUs extends AppCompatActivity implements View.OnClickListener {

    //method for the on click buttons
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        Button btnLocation = (Button) findViewById(R.id.btnLocation);
        Button btnFind = (Button) findViewById(R.id.btnFind);

        btnLocation.setOnClickListener(ContactUs.this);
        btnFind.setOnClickListener(ContactUs.this);
    }

    //switch case for the on click buttons within the page
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnLocation:
                startActivity(new Intent(ContactUs.this, MapsActivity.class));
                break;
            case R.id.btnFind:
                startActivity(new Intent(ContactUs.this, FindLocation.class));
                break;

        }
    }

}

