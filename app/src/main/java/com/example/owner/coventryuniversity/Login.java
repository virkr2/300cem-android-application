package com.example.owner.coventryuniversity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Login extends AppCompatActivity implements View.OnClickListener {
    private Button btnLogout;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        session = new Session(this);
        if (!session.loggedin()) {
            logout();
        }

        //each image button defined
        ImageButton btnAboutus = (ImageButton) findViewById(R.id.btnAboutUs);
        ImageButton btnGallery = (ImageButton) findViewById(R.id.btnGallery);
        ImageButton btnPhoto = (ImageButton) findViewById(R.id.btnPhoto);
        ImageButton btnContactUs = (ImageButton) findViewById(R.id.btnContactus);
        Button btnLogout = (Button) findViewById(R.id.btnLogout);
        btnLogout = (Button) findViewById(R.id.btnLogout);

        btnAboutus.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnPhoto.setOnClickListener(this);
        btnContactUs.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    //method for the logout function
    private void logout() {
        session.setLoggedin(false);
        finish();
        startActivity(new Intent(Login.this, MainActivity.class));
    }

    //method for the overlay menu in the top right corner
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //specifies the three links in the overlay menu
        if (id == R.id.action_speech) {
            startActivity(new Intent(Login.this, Speech.class));
        }
        if (id == R.id.action_timer) {
            startActivity(new Intent(Login.this, Stopwatch.class));
        }
        if (id == R.id.action_calculator) {
                startActivity(new Intent(Login.this, calculator.class));
        }


        return super.onOptionsItemSelected(item);
    }

    //switch case for the menu for each on click image button
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnAboutUs:
                startActivity(new Intent(Login.this, AboutUs.class));
                break;
            case R.id.btnGallery:
                startActivity(new Intent(Login.this, Gallery.class));
                break;
            case R.id.btnPhoto:
                startActivity(new Intent(Login.this, Photo.class));
                break;
            case R.id.btnContactus:
                startActivity(new Intent(Login.this, ContactUs.class));
                break;
            case R.id.btnLogout:
                startActivity(new Intent(Login.this, MainActivity.class));
                break;
        }
    }
}