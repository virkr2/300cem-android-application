package com.example.owner.coventryuniversity;

public class Building {
    private String name;
    private String detail;
    private int photo;

    //method for building name, detail and photo
    public Building(String name, String detail, int photo) {
        this.name = name;
        this.detail = detail;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public int getPhoto() {
        return photo;
    }

    @Override
    public String toString() {
        return detail;
    }
}