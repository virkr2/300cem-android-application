package com.example.owner.coventryuniversity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class GridActivity extends AppCompatActivity {

    //method for the grid view of images
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        GridView gridview = (GridView) findViewById(R.id.activity_grid);
        gridview.setAdapter(new ImageAdapter(this));

        //labels the photo and defines which position they are in
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(getBaseContext(), "At position " + position + " is " + getResources().getStringArray(R.array.buildingNames)[position], Toast.LENGTH_SHORT).show();
            }
        });
    }
}