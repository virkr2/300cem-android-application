package com.example.owner.coventryuniversity;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button login, register;
    private EditText etEmail, etPass;
    private DbHelper db;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //buttons defined with on click method
        db = new DbHelper(this);
        session = new Session(this);
        login = (Button) findViewById(R.id.btnLogin);
        register = (Button) findViewById(R.id.btnReg);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass = (EditText) findViewById(R.id.etPass);
        login.setOnClickListener(this);
        register.setOnClickListener(this);

        if (session.loggedin()) {
            startActivity(new Intent(MainActivity.this, Login.class));
            finish();
        }
    }

    //switch case for the login and registration
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
            case R.id.btnReg:
                startActivity(new Intent(MainActivity.this, SignUp.class));
                break;
            default:

        }
    }


    //login will start activity therefore open menu page
    private void login() {
        String email = etEmail.getText().toString();
        String pass = etPass.getText().toString();

        if (db.getUser(email, pass)) {
            session.setLoggedin(true);
            startActivity(new Intent(MainActivity.this, Login.class));

            overridePendingTransition(R.anim.animation, R.anim.animation2);

            finish();
        } else {
            //Error message will appear if the details are not registered correctly
            Toast.makeText(getApplicationContext(), "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
        }
    }

}


