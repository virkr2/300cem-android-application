package com.example.owner.coventryuniversity;

//This test checks if the user can log in correctly and checks if it opens the main menu


import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EspressoTest {

    @Rule
    public ActivityTestRule<Login> activityTestRule = new ActivityTestRule<>(
            Login.class);

    @Test
    public void changeText_MainActivity() {
        onView(withId(R.id.etEmail)).perform(typeText("ran"),closeSoftKeyboard());
        onView(withId(R.id.etPass)).perform(typeText("123"),closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());

    }

}