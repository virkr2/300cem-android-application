**Coventry University Mobile Application**

- CU Mobile App is a University application that is useful for all Coventry University students old and new. This app features a gallery of the campus, a camera feature, a GPS location of the University, a speech to text converter, an about us page and a unique login.

- Unique and key features include data persistence which ensures the user will remain logged in once an account is created, another feature includes a GPS location marker which once activated will open Google Maps to show a live view of the Coventry University campus, another feature includes a speech to text converter which allows a user to speak instead of type notes or phrases, a basic calculator function, a stopwatch and another feature includes a gallery with a variety of views including a grid view and list view.

- Overall, this has been and is a highly educational project and I have learnt a vast amount in terms of creating an Android application and JAVA coding. However I would also like to improve the application and add further features which would progress the application even further, some of these have been discussed within the video submission.

- This repository contains the source code of the Coventry University App for Android.